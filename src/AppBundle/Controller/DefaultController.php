<?php

namespace AppBundle\Controller;

use AppBundle\Event\ImageEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{


    /**
     * @return array
     *
     * @Route("/", name="frontend_albums_list")
     * @Template("AppBundle:Album:list.html.twig")
     * @Method("GET")
     */
    public function albumAction()
    {
        $em = $this->getDoctrine()->getManager();
        $albums = $em->getRepository("AppBundle:Album")->findAll();

        return [
            'albums' => $albums
        ];
    }

    /**
     * @Method("GET")
     * @Route("/albums", name="frontend_albums_json")
     *
     */
    public function albumsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $albums = $em->getRepository("AppBundle:Album")->findAll();

        $serializer = $this->get('jms_serializer');
        $response = new Response($serializer->serialize($albums, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

	/**
	 * @Method("GET")
	 * @Route("/shortalbums", name="frontend_shortalbums_json")
	 *
	 */
	public function shortAlbumsAction()
	{
	  $em = $this->getDoctrine()->getManager();
	  $albums = $em->getRepository("AppBundle:Album")->findAlbums();
	  $serializer = $this->get('jms_serializer');
	  $response = new Response($serializer->serialize($albums, 'json'));
	  $response->headers->set('Content-Type', 'application/json');

	  return $response;
	}

    /**
     * @Method("GET")
     * @Route(
     *     "/album/{id}/{page}",
     *     	name="frontend_album_item",
	 * 		defaults={ "page": 1 },
     *     	requirements={ "id": "\d+", "page": "\d+" }
     * )
     *
     */
    public function albumItemAction($id, $page, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $album = $em->getRepository("AppBundle:Album")->findOneBy(['id' => $id]);

        if (!$album) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

	  	$query = $em->getRepository("AppBundle:Image")->findAlbumImages($album);

		$event = new ImageEvent($album, $query, $page);
		$dispatcher = $this->container->get('event_dispatcher');
		$dispatcher->dispatch('app.image_listener', $event);

        return $event->getResponse();
    }

}
