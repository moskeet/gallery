<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ImageEvent extends Event
{
  protected $album;
  protected $images;
  protected $page;
  protected $response;

  public function __construct($album, $images, $page, $response = '')
  {
	$this->album = $album;
	$this->images = $images;
	$this->page = $page;
	$this->response = $response;
  }

  public function getPage()
  {
	return $this->page;
  }

  public function getAlbum()
  {
	return $this->album;
  }

  public function getImages()
  {
	return $this->images;
  }

  public function setResponse($response)
  {
	$this->response = $response;
	return $this;
  }

  public function getResponse()
  {
  	return $this->response;
  }

}