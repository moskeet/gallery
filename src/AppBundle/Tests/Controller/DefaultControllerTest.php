<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testAlbum()
    {
	  $client = static::createClient();
	  $client->request('GET', '/albums');
	  $response = $client->getResponse();
	  $this->assertSame(200, $response->getStatusCode());
	  $albums = json_decode($response->getContent(), true);
	  $this->assertCount (5, $albums);
    }

  	public function testShortAlbums()
	{
	  $client = static::createClient();
	  $client->request('GET', '/shortalbums');
	  $response = $client->getResponse();
	  $this->assertSame(200, $response->getStatusCode());
	  $albums = json_decode($response->getContent(), true);
	  $this->assertCount (1, $albums);
	}

}
