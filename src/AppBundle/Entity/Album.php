<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlbumRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Album
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @JMS\Expose
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="album", cascade={"all"}, orphanRemoval=true)
     *
     */
    protected $imagesArray;

    public function __construct()
    {
        $this->imagesArray = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Album
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add imagesArray
     *
     * @param Images $imagesArray
     */
    public function addImages(Images $imagesArray)
    {
        $this->imagesArray[] = $imagesArray;
    }

    /**
     * @return mixed
     */
    public function getImagesArray()
    {
        return $this->imagesArray;
    }

}

