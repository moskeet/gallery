<?php

namespace AppBundle\EventListener;

use AppBundle\Event\ImageEvent;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ImageListener
{

  	protected $container;

	public function __construct(\Symfony\Component\DependencyInjection\Container $container)
	{
	  $this->container = $container;
	}

	public function onImageEvent(ImageEvent $event)
	{
		$album = $event->getAlbum();

		$paginator = $this->container->get('knp_paginator');

	  	$images = $paginator->paginate(
		  $event->getImages(),
		  (int) $event->getPage(),
		  10
		);

		$serializer = $this->container->get('jms_serializer');
		$response = new Response($serializer->serialize(['images' => $images, 'album' => $album], 'json'));
		$response->headers->set('Content-Type', 'application/json');

	  	$event->setResponse($response);

	}

}