AlbumManager = new Marionette.Application()

AlbumManager.addRegions(
  mainRegion: '#main-region'
)

AlbumManager.navigate = (route, options) ->
  options || (options = {})
  Backbone.history.navigate(route, options)
  return

AlbumManager.getCurrentRoute = ->
  Backbone.history.fragment

AlbumManager.on "start", ->
  if (Backbone.history)
    Backbone.history.start()
    if (this.getCurrentRoute() == '')
      AlbumManager.trigger "albums:list"
  return
