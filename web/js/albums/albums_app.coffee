@AlbumManager.module "AlbumsApp", (AlbumsApp, AlbumManager, Backbone, Marionette, $, _) ->

  class AlbumsApp.Router extends Marionette.AppRouter
    appRoutes:
      'albums': 'listAlbums'
      'album/:id': 'showAlbum'
      'album/:id/page/:page': 'showImages'
      'shortalbums': 'shortAlbums'

  API =
    listAlbums: ->
      AlbumManager.AlbumsApp.List.Controller.listAlbums()
    showAlbum: (id) ->
      AlbumsApp.Show.Controller.showAlbum(id)
    showImages: (id, page) ->
      AlbumsApp.Show.Controller.showImages(id, page)
    shortAlbums: ->
      AlbumManager.AlbumsApp.List.Controller.listShortAlbums()

  AlbumManager.on "albums:list", ->
    AlbumManager.navigate('albums')
    API.listAlbums()
    return

  AlbumManager.on "shortalbums:list", ->
    AlbumManager.navigate('shortalbums')
    API.listAlbums()
    return

  AlbumManager.on "album:show", (id) ->
    AlbumManager.navigate('album/' + id)
    API.showAlbum(id)
    return

  AlbumManager.on "before:start", ->
    new AlbumsApp.Router( controller: API )
    return


  return