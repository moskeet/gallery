@AlbumManager.module "AlbumsApp.Show", (Show, AlbumManager, Backbone, Marionette, $, _) ->

  Show.Controller =
    showAlbum: (id) ->
      album = AlbumManager.request 'album:entity', id
      albumView = new Show.GetAlbumView( collection: album )
      AlbumManager.mainRegion.show(albumView)
      return
    showImages: (id, page) ->
      images = AlbumManager.request 'image:entity', id, page
      imagesView = new Show.GetAlbumView( collection: images );

      AlbumManager.mainRegion.show(imagesView);
      return


  return