@AlbumManager.module "AlbumsApp.Show", (Show, AlbumManager, Backbone, Marionette, $, _) ->

  class Show.MissingAlbum extends Marionette.ItemView
    template: '#missing-album-view'

  class Show.Album extends Marionette.ItemView
    template: '#album-view'

  class Show.UniqueAlbumView extends Backbone.Marionette.ItemView
    template: "#album-view"
    page_count: null
    page_active: null
    page_show: 10
    initialize: ->
      this.page_count = @model.set(page_count: @model.get('images').total_count)
      this.page_show = @model.set(page_show: Math.round(@model.get('images').total_count / @model.get('images').num_items_per_page));
      this.page_active =  @model.set(page_active: @model.get('images').current_page_number);
      return

  class Show.GetAlbumView extends Backbone.Marionette.CollectionView
    tagName: 'div'
    childView: Show.UniqueAlbumView
    viewOptions:
      page_count: 3
