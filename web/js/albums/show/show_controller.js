// Generated by CoffeeScript 1.10.0
(function() {
  this.AlbumManager.module("AlbumsApp.Show", function(Show, AlbumManager, Backbone, Marionette, $, _) {
    Show.Controller = {
      showAlbum: function(id) {
        var album, albumView;
        album = AlbumManager.request('album:entity', id);
        albumView = new Show.GetAlbumView({
          collection: album
        });
        AlbumManager.mainRegion.show(albumView);
      },
      showImages: function(id, page) {
        var images, imagesView;
        images = AlbumManager.request('image:entity', id, page);
        imagesView = new Show.GetAlbumView({
          collection: images
        });
        AlbumManager.mainRegion.show(imagesView);
      }
    };
  });

}).call(this);
