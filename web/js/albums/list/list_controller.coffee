@AlbumManager.module "AlbumsApp.List", (List, AlbumManager, Backbone, Marionette, $, _) ->

  List.Controller =
    listAlbums: ->
      albums = AlbumManager.request "album:entities"
      albumsListView = new List.Albums( collection: albums )
      albumsListView.on "childview:album:show", (childView, model) ->
        AlbumManager.trigger "album:show", model.get('id')
        return

      AlbumManager.mainRegion.show(albumsListView)
    listShortAlbums: ->
      albums = AlbumManager.request "shortalbums:entities"
      shortAlbumsListView = new List.Albums( collection: albums )
      shortAlbumsListView.on "childview:album:show", (childView, model) ->
        AlbumManager.trigger "album:show", model.get('id')
        return

      AlbumManager.mainRegion.show(shortAlbumsListView)

  return