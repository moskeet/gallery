@AlbumManager.module "AlbumsApp.List", (List, AlbumManager, Backbone, Marionette, $, _) ->

  class List.Album extends Marionette.ItemView
    tagName: 'tr'
    template: '#albums-list-item'
    events:
      "click td a.js-show": "showClicked"
    showClicked: (e) ->
      e.preventDefault()
      this.trigger "album:show", @model
      return


  class List.Albums extends Marionette.CollectionView
    tagName: 'table'
    childView: List.Album

  return