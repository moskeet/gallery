@AlbumManager.module "Entities", (Entities, AlbumManager, Backbone, Marionette, $, _) ->

  class Entities.Album extends Backbone.Model
      urlRoot: '/album'

  class Entities.AlbumCollection extends Backbone.Collection
    url: '/albums'
    model: Entities.Album
    comparator: (album) ->
        album.get('title')

  class Entities.ShortAlbumsCollection extends Backbone.Collection
    url: '/shortalbums'
    model: Entities.Album
    comparator: (album) ->
      album.get('title')

  class Entities.GetAlbum extends Backbone.Model

  class Entities.GetAlbumCollection extends Backbone.Collection
    model: Entities.GetAlbum
    initialize: (models, options) ->
        this.urlParam = if (options.page) then options.id + '/' + options.page else options.id
        return
    url: ->
        '/album/' + this.urlParam

  API =
    getAlbumEntities: ->
      albums = new Entities.AlbumCollection()
      albums.fetch()
      albums
    getAlbumEntity: (albumId) ->
      album = new Entities.GetAlbumCollection([], {id: albumId})
      album.fetch()
      album
    getImagesEntity: (albumId, pageId) ->
      images = new Entities.GetAlbumCollection([], {id: albumId, page: pageId})
      images.fetch()
      images
    getShortAlbumsEntities: ->
      albums = new Entities.ShortAlbumsCollection()
      albums.fetch()
      albums

  AlbumManager.reqres.setHandler "album:entities", ->
    API.getAlbumEntities()
  AlbumManager.reqres.setHandler "shortalbums:entities", ->
    API.getShortAlbumsEntities()
  AlbumManager.reqres.setHandler "album:entity", (id) ->
    API.getAlbumEntity(id)
  AlbumManager.reqres.setHandler "image:entity", (id, page) ->
    API.getImagesEntity(id, page)

  return
